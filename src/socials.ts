import fetch from 'node-fetch';
import { removeQuerystringFromIdPath } from './common';
import { environment } from './environment';
import { Collections, Entity, MetaKey } from './type-defs';
import { getMetadataOfKey } from './parsers/entities';

const renderPugPage = (
  res: any,
  page: string,
  title: string,
  description: string,
  image: string,
  image_height: string,
  image_width: string,
  reqUrl: string
) => {
  res.render(page, {
    title,
    description,
    image,
    image_height,
    image_width,
    reqUrl,
  });
};

const getMetaTagTitle = (entity: Entity | undefined): string => {
  const fallback = 'Collectie van de Gentenaar';
  if (entity) {
    return (
      getMetadataOfKey(entity, MetaKey.Title)?.value ||
      getMetadataOfKey(entity, MetaKey.Fullname)?.value ||
      fallback
    );
  }
  return fallback;
};

const getMetaTagDescription = (entity: Entity | undefined): string => {
  const fallback =
    'Wat als burgers, musea en erfgoedinstellingen vrij en creatief gebruik konden maken van elkaars objecten, ...';
  if (entity) {
    return getMetadataOfKey(entity, MetaKey.Description)?.value || fallback;
  }
  return fallback;
};

const getMetaTagImage = (entity: Entity | undefined) => {
  const fallback = '';
  if (entity) {
    return (
      entity.primary_transcode_location ||
      entity.primary_mediafile_location ||
      ''
    );
  }
  return fallback;
};

export const handleSocialCrawlers = async (req: any, res: any) => {
  try {
    const uri = req.query.request_uri as string;
    const idPath = uri.split('/').reverse()[0];
    const entityId = removeQuerystringFromIdPath(idPath);
    const response = await fetch(
      `${environment.api.collectionAPIUrl}/${Collections.Entities}/${entityId}`,
      {
        method: 'get',
        headers: { Authorization: 'Bearer ' + environment.staticToken },
      }
    );
    const entity = (await response.json()) as Entity;
    renderPugPage(
      res,
      'socials',
      getMetaTagTitle(entity),
      getMetaTagDescription(entity),
      getMetaTagImage(entity),
      entity.primary_height?.toString() || '',
      entity.primary_width?.toString() || '',
      environment.webPortal + req.url
    );
  } catch (e) {
    renderPugPage(
      res,
      'socials',
      getMetaTagTitle(undefined),
      getMetaTagDescription(undefined),
      getMetaTagImage(undefined),
      '',
      '',
      environment.webPortal + req.url
    );
  }
};
